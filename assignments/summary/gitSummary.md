# Git Summary

## What is git and why?
        Git is a software that makes the work very easy.You can easily collaborate with your teammates in git for the work.Let us have a look on the basics that required for the git.  

# Basics 
*Repository:*
    It can also be called as repo.It is the collection of files and folders.This is the box where you and your team will store the code.
*GitLab:*   
    Remote storage solution for git repositories.
*Commit:*
    Commit is for saving your work.So, it is very important to remember otherwise the work you have done will not be saved.It wiil exist on your local machine until it is pushed into remote repository.
*Push:*
    Pushing is essentially syncing your commits to GitLab.
*Branch:*
    Here you can take can take tree as an example .The trunk is the main software and it is called as the Master Branch.THe branches are the separate instances of the code.
*Merge:*
    Connecting the two branches(which are free from bugs) together.
*Clone:*
    Cloning is nothing but getting the exact copy of the entire online repository on your local machine.
*Fork:*
    Forking is just like cloning,instead of making a exact copy of an existing repo on your local machine,you get an entirely new repo of that code under your own name.

## How to install Git   
For Linux, open the terminal and type
```bash
    $ sudo apt-get install git
```
# Git has 3 main states

1. Modified means that you have changedfile but have not committed it your repo yet.
2. Staged means that you have marked a modified file in its current version to go into your next picture.
3. Committed means that the data is safely stored in your local repo in form of pictures.

# Git Workflow: 
![workflow:](extras/Git.png) 
1. Workspace:All the changes you make via Editors(gedit,notepad,vim,nano)is done in this tree of repo.

2. Staging:All thestaged files go into this tree of your repo.

3. Local Repository:All the committed go to this tree of your repo.

4. Remote Repository:This is the copy of your local repository but is stored in some server on the internet.All the changes you commit into the local repository are not directly reflected into this tree.You need to push your changes to the Remote Repository.
    
*You clone the repo
```bash
    $ git clone <link-to-repository>
```

*Create a new branch
```bash
    $ git checkout Master
    $ git checkout -b <your-branch-name>
```

1. You modify files in your working tree.

2. You selectively stage just those changes you want to be part of your next commit,which adds only those changes to the staging area.
```bash
    $ git add .           # To add untracked files( . adds all files)
```

1. You do a commit,which takes the flies as they are in the stagging area and stores the snapshot permanently to your Local Git Repository.
```bash
    $ git commit -sv      # Description about the commit
```
1. You do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.
```bash
    $ git push origin <branch-name>       #push changes into repo
```




